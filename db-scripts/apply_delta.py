#!/usr/bin/env python

import sqlite3
import csv
import sys
import os

if len(sys.argv) != 2:
	print("%s [delta-basedir]" % sys.argv[0])
	sys.exit(1)

basedir = sys.argv[1]

def get_pk_where(conn, urow, table):
	c = conn.cursor()
	c.execute('pragma table_info(%s)' % table)
	pks = []
	i = 0
	while True:
		uval = urow[i]
		i += 1
		col = c.fetchone()
		if col == None:
			break
		if col[-1] == 1:
			cond = "%s = '%s'" % (col[1], uval)
			pks.append(cond)
	return ' AND '.join(pks)

def get_col_count(conn, table):
	c = conn.cursor()
	c.execute('pragma table_info(%s)' % table)
	i = 0
	while None != c.fetchone():
		i += 1
	return i

def get_update_query_for(conn, table, urow):
	c = conn.cursor()
	c.execute('pragma table_info(%s)' % table)
	sets = []
	pks = []
	nurow = []
	for uval in urow:
		col = c.fetchone()
		if col[-1] == 1:
			cond = "%s = '%s'" % (col[1], uval)
			pks.append(cond)
		else:
			cond = "%s = ?" % (col[1])
			sets.append(cond)
			nurow.append(uval)
	cols = ', '.join(sets)
	where = ' AND '.join(pks)
	return 'UPDATE %s SET %s WHERE %s' % (table, cols, where), nurow

def import_delta_csv_to_table(conn, filename, table, has_old_postalcode=False):
	filepath = "%s/%s" % (basedir, filename)
	if not os.path.exists(filepath):
		return
	c = conn.cursor()
	print("Applying '%s' into '%s'..." % (filepath, table))
	with open(filepath, 'r') as csvfile:
		reader = csv.reader(csvfile, delimiter='@')
		for row in reader:
			urow = [txt.decode('iso-8859-1') for txt in row]
			rev = 1
			if has_old_postalcode:
				rev = 2
				old_postalcode = urow[len(urow)-1] # dont know how to use this
			operation = row[len(row)-rev]
			idwhere = get_pk_where(conn, urow, table)
			colcount = get_col_count(conn, table)
			interrogs = ', '.join(['?'] * colcount)
			urow = urow[:colcount-len(urow)]
			try:
				if operation == 'DEL':
					query = "DELETE FROM %s WHERE %s" % (table, idwhere)
					#print query, urow
					c.execute(query)
				elif operation == 'INS':
					query = "INSERT INTO %s VALUES (%s)" % (table, interrogs)
					#print query, urow
					c.executemany(query, [urow])
				elif operation == 'UPD':
					query, urow = get_update_query_for(conn, table, urow)
					#print query, urow
					c.executemany(query, [urow])
			except sqlite3.Error as e:
				print '=' * 70
				print "Error: ", e, filename, table
				print query, urow
				print '=' * 70

	conn.commit()

conn = sqlite3.connect('dne.db', isolation_level=None)
c = conn.cursor()
c.execute('PRAGMA foreign_keys = ON')

import_delta_csv_to_table(conn, 'DELTA_LOG_FAIXA_UF.TXT', 'log_faixa_uf')
import_delta_csv_to_table(conn, 'DELTA_LOG_LOCALIDADE.TXT', 'log_localidade', True)
import_delta_csv_to_table(conn, 'DELTA_LOG_FAIXA_LOCALIDADE.TXT', 'log_faixa_localidade')
import_delta_csv_to_table(conn, 'DELTA_LOG_VAR_LOC.TXT', 'log_var_loc')
import_delta_csv_to_table(conn, 'DELTA_LOG_BAIRRO.TXT', 'log_bairro')
import_delta_csv_to_table(conn, 'DELTA_LOG_FAIXA_BAIRRO.TXT', 'log_faixa_bairro')
import_delta_csv_to_table(conn, 'DELTA_LOG_VAR_BAI.TXT', 'log_var_bai')
import_delta_csv_to_table(conn, 'DELTA_LOG_CPC.TXT', 'log_cpc', True)
import_delta_csv_to_table(conn, 'DELTA_LOG_FAIXA_CPC.TXT', 'log_faixa_cpc')
import_delta_csv_to_table(conn, 'DELTA_LOG_LOGRADOURO.TXT', 'log_logradouro', True)
import_delta_csv_to_table(conn, 'DELTA_LOG_VAR_LOG.TXT', 'log_var_log')
import_delta_csv_to_table(conn, 'DELTA_LOG_NUM_SEC.TXT', 'log_num_sec')
import_delta_csv_to_table(conn, 'DELTA_LOG_GRANDE_USUARIO.TXT', 'log_grande_usuario', True)
import_delta_csv_to_table(conn, 'DELTA_LOG_UNID_OPER.TXT', 'log_unid_oper', True)
import_delta_csv_to_table(conn, 'DELTA_LOG_FAIXA_UOP.TXT', 'log_faixa_uop')

conn.close()
