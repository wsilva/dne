#!/usr/bin/env python

import sqlite3
import csv
import sys
import os

basedir = 'exported'

def export_table_to_csv(conn, table):
	filename = '%s.TXT' % table.upper()
	filepath = "%s/%s" % (basedir, filename)
	c = conn.cursor()
	print("Exporting '%s' into '%s'..." % (filepath, table))
	with open(filepath, 'w') as csvfile:
		writer = csv.writer(csvfile, delimiter='@')
		c.execute('SELECT * FROM %s' % table)
		while True:
			urow = c.fetchone()
			if urow == None:
				break
			row = [('%s' % col).encode('iso-8859-1') for col in urow]
			writer.writerow(row)

conn = sqlite3.connect('dne.db')
c = conn.cursor()
c.execute('PRAGMA foreign_keys = ON')

export_table_to_csv(conn, 'log_bairro')
export_table_to_csv(conn, 'log_cpc')
export_table_to_csv(conn, 'log_faixa_bairro')
export_table_to_csv(conn, 'log_faixa_cpc')
export_table_to_csv(conn, 'log_faixa_localidade')
export_table_to_csv(conn, 'log_faixa_uf')
export_table_to_csv(conn, 'log_faixa_uop')
export_table_to_csv(conn, 'log_grande_usuario')
export_table_to_csv(conn, 'log_localidade')
export_table_to_csv(conn, 'log_logradouro')
export_table_to_csv(conn, 'log_num_sec')
export_table_to_csv(conn, 'log_unid_oper')
export_table_to_csv(conn, 'log_var_bai')
export_table_to_csv(conn, 'log_var_loc')
export_table_to_csv(conn, 'log_var_log')

conn.close()
