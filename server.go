// Copyright 2013 Zocprint Serviços Gráficos Ltda. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	_ "net/http/pprof"
	"time"
)

func setupServer() *http.Server {
	router := mux.NewRouter()
	router.HandleFunc(CEP_URL, cepHandler)
	router.HandleFunc(QUICK_CEP_URL, quickCEPHandler)
	http.Handle("/", router)

	server := &http.Server{
		Addr:         fmt.Sprintf("%s:%d", listenIP, listenPort),
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	if debug {
		go func() {
			log.Println(http.ListenAndServe("localhost:6060", nil))
		}()
	}

	return server
}
